package com.conversionapp;

import org.springframework.web.bind.annotation.*;

@RestController
public class MetricImperialConversionController {



    //convert Fahrenheit to Celsius
    //C = (F-32)/1.8
    //http://localhost:8080/convertToCelsius?fahrenheit=22.0
    @GetMapping("/convertToCelsius")
    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseObject getCelsius(@RequestParam(required = true) double fahrenheit) {
            double val = (fahrenheit-32.0)/1.8;
            return new ResponseObject(val);
    }


    //convert Celsius to Fahrenheit
    //F = C*1.8+32
    @GetMapping("/convertToFahrenheit")
    public ResponseObject getFahrenheit(@RequestParam(required = true) double celsius) {
        double value =  (celsius*1.8)+32.0;
        return new ResponseObject(value);
    }

    //convert miles to km
    //km = miles*1.609344
    @GetMapping("/convertToKilometres")
    public ResponseObject getKilometres(@RequestParam(required = true) double miles) {
        double value =  miles*1.609344;
        return new ResponseObject(value);
    }


    //convert km to miles
    //miles = km /1.609344;
    @GetMapping("/convertToMiles")
    public ResponseObject getMiles(@RequestParam(required = true) double kilometers) {
        double value =  kilometers/1.609344;
        return new ResponseObject(value);
    }


    //convert pounds to Kg
    //kg = pounds/2.2046
    @GetMapping("/convertToKilograms")
    public ResponseObject getKilograms(@RequestParam(required = true) double pounds) {
        double value =  pounds/2.2046;
        return new ResponseObject(value);
    }


    //convert kg to pounds
    //pounds = kg * 2.2046
    @GetMapping("/convertToPounds")
    public ResponseObject getPounds(@RequestParam(required = true) double kilograms) {
        double value =  kilograms*2.2046;
        return new ResponseObject(value);
    }



    //convert metres to yards
    //yards = meters * 1.0936
    @GetMapping("/convertToYards")
    public ResponseObject getYards(@RequestParam(required = true) double meters) {
        double value  = meters*1.0936;
        return new ResponseObject(value);
    }



    //convert yards to metres
    //meters = yards/1.0936
    @GetMapping("/convertToMeters")
    public ResponseObject getMeters(@RequestParam(required = true) double yards) {
        double value =  yards/1.0936;
        return new ResponseObject(value);
    }

}